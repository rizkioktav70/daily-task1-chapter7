import HeaderNavbar from './components/Header';
import Card from './components/Card';
import Heading from './components/HeadingTitle';

import 'bootstrap/dist/css/bootstrap.min.css'
import './css/app/App.css';

function App() {
  return (
    <div className="App">
      <HeaderNavbar />
      <div className="Main">
        <Heading title="Hello FSW13" />
        <Card
          title="Hello"
          description="Lorem ipsum dolor sit amet"
          btnText="Go Somewhere"
          btnHref="https://google.com"
          imgSrc="https://placeimg.com/320/240/any"
          imgAlt="Hello"
        />
      </div>
    </div>
  );
}

export default App;
