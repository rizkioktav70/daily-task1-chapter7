import React from "react";

const HeadingTitle = (props) => {
    const { title } = props;

    return (
        <div className="heading" style={{ width: "18rem" }}>
            <div className="card-body">
                <h1 className="card-title">{title}</h1>
            </div>
        </div>
    )
}

export default HeadingTitle;